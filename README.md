Supplement to 
### Mammalian oocytes store proteins for the early embryo on cytoplasmic lattices
Jentoft, I.M.A., Baeuerlein, F.J.B.,  Welp, L.M.,  Cooper, B.H., Petrovic, I., So, C.,
Penir, S., Politi, A.Z., Horokhovskyi, Y., Takala, I., Eckel, H.,  Moltrecht, R., Lenart, P., 
Cavazza, T., Liepe, J., Brose, N., Urlaub, H., Fernandez-Busnadiego, R., 
and Schuh, M.\
Cell 186. 1-20, November 22, 2023, https://doi.org/10.1016/j.cell.2023.10.003
 

## Computation of measurement length error induced by expansion microscopy


[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.8359490.svg)](https://doi.org/10.5281/zenodo.8359490)

The code in this repository computes a metric to quantify distortions and anisotropies induced by expansion microscopy (*post*) 
with respect to unexpanded (*pre*) samples. We sequentially perform different rigid and non-rigid transformation and compare the results. Method is as used in [Chen and Boyden, Science 2015](https://www.science.org/doi/10.1126/science.1260088) and
[M`Saad and Bewersdorf, Nat. Comm. 2020](https://www.nature.com/articles/s41467-020-17523-8). 

## Data preparation
You need to provide region in pre and post ExM images that nmatch to each other. 

### Pre-orientation

It is very useful to pre-rotate the post-ExM image so that the registrion achieve a global minima. It is assumed that the images are XYZ.

For instance you can run in ImageJ simple rotations by 90 degrees `[Image > Tranform > Rotate ...]` 
(this is only between -90 and 90), `[Image › Transform › Rotate 90 Degrees Left]` and  
`[Image › Transform › Rotate 90 Degrees Right]` if needed for larger angles. 


### Define and match  regions

* For each channel of interest (MTs and DNA/chromosomes) define corresponding ROIs in *pre* and *post* images. Name the ROIs `pre_1, pre_2, pre_3, ...`  and `post_1, post_2, ...`, respectively. 

* Save the ROIs of *pre* and *post* into separate `zip` files respectively (e.g. `FOV_post_MT.zip`). 

> Make sure that the ROIs are in the correct order so that the image pairs match. Apply a sort of the names if needed `[ROIManager >More > Sort]`

### Save field of views as separate images
Run the script `create_FOV.ijm` and specifiy the corresponding images and ROI `zip` files. The script will save field of views as 
* `FOV1_pre.tif`, `FOV1_post.tif`, these are single planes
* `FOV1_MAXn_pre.tif`, `FOV1_MAXn_post.tif`, these are a maximum intensity projection `2*n+1` slices around the manually selected slice.



## Run the registration

For registration we use `Elastix`[^1][^2] with the Python API `ITK-Elastix`[^3]. Compute the registration using the two provide jupyter notebooks. Verify that indeed the registration converges. If you see that the registration did not work try different PRE_SCALE factor or better starting orientation (**Data preparation**)

## Compute distance metric
Use the R code to compute the distance metric and measurement length error.



 
## Author 
Antonio Politi apoliti@mpinat.mpg.de

July 2023, MPI-NAT Goettingen

## References

[^1]: S. Klein, M. Staring, K. Murphy, M.A. Viergever, J.P.W. Pluim, "elastix: a toolbox for intensity based medical image registration," IEEE Transactions on Medical Imaging, vol. 29, no. 1, pp. 196 - 205, January 2010. 

[^2]: D.P. Shamonin, E.E. Bron, B.P.F. Lelieveldt, M. Smits, S. Klein and M. Staring, "Fast Parallel Image Registration on CPU and GPU for Diagnostic Classification of Alzheimer's Disease", Frontiers in Neuroinformatics, vol. 7, no. 50, pp. 1-15, January 2014.

[^3]: InsightSoftwareConsortium/ITKElastix: ITKElastix 0.15.0 [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7378570.svg)](https://doi.org/10.5281/zenodo.7378570)
