# @ File (label="Pre expansion image") pre 
# @ File (label="Post expansion image") post 
# @ File roi_file_pre
# @ File roi_file_post
# @ File (style = 'directory') outdir 

run("Close All");
save_fov(pre, roi_file_pre, "pre");
save_fov(post, roi_file_post, "post");
save_fov_project(pre, roi_file_pre, "pre", 1);
save_fov_project(post, roi_file_post, "post", 1);

function save_fov(image_name, roi_file, postfix){
	run("Close All");
	run("ROI Manager...");
	n = roiManager('count');
	if (n  > 0){ 
		roiManager("Deselect");
		roiManager("Delete");
	}
	open(image_name);
	rename(postfix);
	open(roi_file);
	
	n = roiManager('count');
	for (i = 0; i < n; i++) {
		selectImage(postfix);
	    roiManager('select', i);
	    run("Duplicate...", "use");
	    fnameout = outdir + File.separator  +"FOV" + toString(i+1) + "_" +  postfix +".tif";
	    saveAs("Tiff", fnameout);
	}

}



function save_fov_project(image_name, roi_file, postfix, zradius){
	// zradius is number of slices above and below to be projected
	run("Close All");
	run("ROI Manager...");
	n = roiManager('count');
	if (n  > 0){ 
		roiManager("Deselect");
		roiManager("Delete");
	}
	open(image_name);
	getDimensions(width, height, channels, slices, frames);
	print("Number of slices " + slices);
	rename(postfix);
	open(roi_file);
	
	n = roiManager('count');
	for (i = 0; i < n; i++) {
		selectImage(postfix);
	    roiManager('select', i);
	    Roi.getPosition(channel, slice, frame);
	    print("Z-position of ROI " + slice);
	    zstart = slice - zradius; 
	    if (zstart < 1) zstart = 1;
	    zend = slice + zradius;
	    if (zend > slices) zend = slices;
	    run("Duplicate...", "duplicate range="+zstart +"-" + zend +" use");
	    run("Z Project...", "projection=[Max Intensity]");
	    fnameout = outdir + File.separator  +"FOV" + toString(i+1) + "_MAX" + zradius + "_" +  postfix +".tif";
	    saveAs("Tiff", fnameout);
	}

}

