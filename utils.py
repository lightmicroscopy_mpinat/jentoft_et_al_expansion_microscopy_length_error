import re
from glob import glob
import pandas as pd
import os
from numpy import unique as unique
from skimage.filters import gaussian
from itk import imread, F, ParameterObject, elastix_registration_method, image_from_array
from imageio.v3 import  improps as io_props

# Patter to recognize filenames of Field of Views
_PATTERN_FOV = 'FOV(?P<id>\d+)_(?P<treat>pre|post).tif'
_PATTERN_FOV_MAX = 'FOV(?P<id>\d+)_MAX\d_(?P<treat>pre|post).tif'

def get_image_pairs(file_path, pattern ):
    """
    Read all tif files in file_path and check for name pattern
    Returns a list of pre and post expansion image pairs
    """
    tif_files = glob(file_path + "*.tif")
    f_ids = list()
    for afile in tif_files:
        basename = os.path.basename(afile)
        m = re.match(pattern, basename)
        if m is not None:
            f_ids.append([ m['id'], m['treat'],afile])
    
    f_ids_pd  = pd.DataFrame(f_ids,  columns = ['id', 'treat', 'fname'])
    ids = unique(f_ids_pd['id'])
    imgnames = list()    
    for aid in ids:
        row = f_ids_pd.index[(f_ids_pd['id']==aid)*(f_ids_pd['treat']=='pre')].to_list()
        prefile = f_ids_pd.iloc[row]['fname'].to_list()[0]
        row = f_ids_pd.index[(f_ids_pd['id']==aid)*(f_ids_pd['treat']=='post')].to_list()
        postfile = f_ids_pd.iloc[row]['fname'].to_list()
        combined_files = [prefile]
        combined_files.extend(postfile)
        imgnames.append(combined_files)
    return(imgnames)

def read_image(img_path, gauss_radius = 0):
    """
    read image and returns an itk type image with scaling
        img_path: full path to image file
        gauss_radius: gauss filter on image, default is no filter
    returns: itk type image
    """
    prop = io_props(img_path, plugin ='tifffile')
    img = imread(img_path, F)
    if gauss_radius == 0:
        itk_img = image_from_array(img)
    else:    
        img_g = gaussian(img, sigma = gauss_radius)
        itk_img = image_from_array(img_g)
    itk_img.SetSpacing([1/prop.spacing[0], 1/prop.spacing[1]])
    return(itk_img)
    
def pre_scale(image, scaling):
    """
    Apply a scaling to an itk image
    This resizes the image spacing
    """
    image.SetSpacing([pix_size/scaling for pix_size in image.GetSpacing()])
    return(image)


def parmeter_transform(number_of_resolutions = 2, bspline_grid_spacing = 2, bspline_weight_regularize = '1'):
    """
    Generate a dictionary containing different transforms parameters
    """
    # Create some default paramater maps
    parameters = ParameterObject.New()
    parameter_map_rigid = parameters.GetDefaultParameterMap('rigid', number_of_resolutions)
    parameter_map_rigid['MaximumNumberOfIterations'] = ['512']
    parameter_map_rigid['MaximumNumberOfSamplingAttemps'] = ['8']
    
    parameter_map_simil = parameters.GetDefaultParameterMap('affine', number_of_resolutions)
    parameter_map_simil['Transform'] = ['SimilarityTransform']
    parameter_map_simil['MaximumNumberOfIterations'] = ['512']
    parameter_map_simil['MaximumNumberOfSamplingAttemps'] = ['8']
    
    parameter_map_affine = parameters.GetDefaultParameterMap('affine', number_of_resolutions)
    parameter_map_affine['MaximumNumberOfIterations'] = ['512']
    parameter_map_affine['MaximumNumberOfSamplingAttemps'] = ['8']
    
    parameter_map_bspline = parameters.GetDefaultParameterMap('bspline', number_of_resolutions, bspline_grid_spacing)
    parameter_map_bspline['MaximumNumberOfIterations'] = ['512']
    parameter_map_bspline['MaximumNumberOfSamplingAttemps'] = ['8']
    parameter_map_bspline['Metric0Weight'] = ['1'] # weight similarity
    parameter_map_bspline['Metric1Weight'] = [bspline_weight_regularize] # weight regularization
    
    # Concatenate parameter maps   
    parameter_object = ParameterObject.New()
    parameter_object.AddParameterMap(parameter_map_rigid)
    parameter_map_last = parameter_map_simil
    parameter_map_last['WriteIterationInfo'] = ['true']
    parameter_object.AddParameterMap(parameter_map_last)
    parameter_object_simil = parameter_object
    
    parameter_object = ParameterObject.New()
    parameter_object.AddParameterMap(parameter_map_rigid)
    parameter_object.AddParameterMap(parameter_map_simil)
    parameter_map_last = parameter_map_affine
    parameter_map_last['WriteIterationInfo'] = ['true']
    parameter_object.AddParameterMap(parameter_map_last)
    parameter_object_affine = parameter_object
    
    parameter_object = ParameterObject.New()
    parameter_object.AddParameterMap(parameter_map_rigid)
    parameter_object.AddParameterMap(parameter_map_simil)
    parameter_map_last = parameter_map_bspline
    parameter_map_last['WriteIterationInfo'] = ['true']
    parameter_object.AddParameterMap(parameter_map_last)
    parameter_object_simil_bspline = parameter_object
    
    parameter_object = ParameterObject.New()
    parameter_object.AddParameterMap(parameter_map_rigid)
    parameter_object.AddParameterMap(parameter_map_simil)
    parameter_object.AddParameterMap(parameter_map_affine)
    parameter_map_last = parameter_map_bspline
    parameter_map_last['WriteIterationInfo'] = ['true']
    parameter_object.AddParameterMap(parameter_map_last)
    parameter_object_affine_bspline = parameter_object
    
    return {'simil': parameter_object_simil, 'affine': parameter_object_affine,
            'simil_bspline': parameter_object_simil_bspline,
            'affine_bspline': parameter_object_affine_bspline}


def find_resolution(fixed, moving, test_resolutions):
    """
    Change resolution of pyramidal images to register moving to fixed
    Find the smallest value of the metric and compare results obtained with similiraty transform
    This is a rough estimate.
    Visual inspection is needed to be sure we found the correct value
    """
    resolution_scan = list()
    for resolution in test_resolutions:
        log_dir = './Iterations/'
        if not os.path.exists(log_dir):
            os.mkdir(log_dir)
        fnames_to_delete = glob(log_dir +'IterationInfo.*')
        for fname in fnames_to_delete:
            os.remove(fname)
        parameter_registration = parmeter_transform(number_of_resolutions = resolution, 
                                                    bspline_grid_spacing =  0.5, bspline_weight_regularize = '1')
        # the bspline parameter here are not used

        result_image_simil, result_transform_simil = elastix_registration_method(
            fixed, moving,
            parameter_object = parameter_registration['simil'],
            output_directory=log_dir,
            log_to_console=True)
        # Find file with iteration file (last iteration)
        fnames_itr = glob(log_dir +'IterationInfo.*')
        #Read last resolution
        iteration_file = pd.read_csv(fnames_itr[-1], sep = '\t')
        final_metric = iteration_file.tail(1)['2:Metric'].values[0]
        resolution_scan.append([resolution, final_metric])
       
    tmp = pd.DataFrame(resolution_scan)
    optimal_resolution = tmp.iloc[tmp.idxmin(axis = 0)[1], 0]
    print(resolution_scan)
    print('optimal resolution: %d' % optimal_resolution)
    return(int(optimal_resolution))


# Utility functions for masking and mask extraction
from imageio.v3 import  imread as io_imread
import os as os
import warnings as warnings
import skimage 
import matplotlib.pyplot as plt
import itk 

def segment_img(img_path, method = 'otsu', gauss_radius = 0, size_thr = 500):
    """
    segment image using authomatic thresholding
        img_path: full path to image
        gauss_radius: radius of pre processing gaussian filtering
        size_thr: remove objects with area below size_thr, in pixels
    returns: a binary mask
    """
    img = utils.read_image(img_path, gauss_radius = gauss_radius)
    if method == 'otsu':
        thr = skimage.filters.threshold_otsu(itk.GetArrayFromImage(img))
    if method == 'li':
        thr = skimage.filters.threshold_li(itk.GetArrayFromImage(img))
    if method == 'local':
        thr = skimage.filters.threshold_local(itk.GetArrayFromImage(img))
    binary = img > thr
    binary = skimage.morphology.binary_opening(binary, skimage.morphology.disk(2))
    labels = skimage.measure.label(binary)
    labels_filtered = skimage.morphology.remove_small_objects(labels, size_thr)
    binary = labels_filtered > 0
    return(binary)

def load_ilastik_segmentation(img_path, size_thr):
    """
    Load the binary mask obtained from ilastik and named 
    "<img_path>_Simple Segmentation.tiff"
        img_path: full path to image
        size_thr: remove objects with area below size_thr, in pixels
    returns: binary mask
    """
    mask_path = os.path.splitext(img_path)[0] + '_Simple Segmentation.tiff'
    if os.path.exists(mask_path):
        binary = io_imread(mask_path) > 1
        labels = skimage.measure.label(binary)
        labels_filtered = skimage.morphology.remove_small_objects(labels, size_thr)
        binary = labels_filtered > 0
        return(binary)
    else:
        warnings.warn("No ilastik mask found for " + img_path, UserWarning)
        return None


def overlap_masks(fixed_path, moving_path, moving_spacing,  result_transform, size_thr ):
    """
    Compute forward transform of binary in fixed image
    Compute overlab (AND operation) of binary in moving image and transformed fixed image
    returns the different masks
    """
    binary_fixed = load_ilastik_segmentation(fixed_path, size_thr = size_thr)
    binary_moving = load_ilastik_segmentation(moving_path, size_thr = size_thr)
    binary_moving = skimage.morphology.dilation(binary_moving)

    itk_binary_moving = itk.GetImageFromArray(binary_moving.astype('uint8'))
    itk_binary_moving.SetSpacing(moving_spacing)
    result_transform.SetParameter('FinalBSplineInterpolationOrder','0')
    binary_moving_transform = itk.transformix_filter(
        itk_binary_moving, result_transform)
    overlap_mask = binary_fixed*binary_moving_transform
    return([overlap_mask, binary_fixed, binary_moving, binary_moving_transform])

def skeleton_region(binary, size_thr):
    # Compute skeletonized version of binary. Perform a size_thr before skeletonization
    # Use for MT
    labels = skimage.measure.label(binary)
    labels_filtered = skimage.morphology.remove_small_objects(labels, size_thr)
    binary = labels_filtered > 0
    binary_out = skimage.morphology.skeletonize(binary)
    reg_prop = skimage.measure.regionprops(binary_out.astype('uint8'))
    return([binary_out, reg_prop])

def boundary_region(binary, size_thr):
    # Compute boundary of binary (external gradient). Perform a size_thr beforehand
    # Use for DNA and spindle
    labels = skimage.measure.label(binary)
    labels_filtered = skimage.morphology.remove_small_objects(labels, size_thr)
    binary = labels_filtered > 0
    binary_out = skimage.morphology.dilation(binary)^(binary)
    reg_prop = skimage.measure.regionprops(binary_out.astype('uint8'))
    return([binary_out, reg_prop])

def test_segment_img(img_path):
    img = io_imread(img_path)
    binary = segment_img(img_path, method = 'li', gauss_radius = 1, size_thr = 100)
    fig, axs = plt.subplots(1, 2, sharey=False,  figsize=[10,10])
    axs[0].imshow(img)
    axs[1].imshow(binary)
    
def test_ilastik_load(img_path):
    img = io_imread(img_path)
    binary = load_ilastik_segmentation(img_path, size_thr = 20)
    fig, axs = plt.subplots(1, 2, sharey=False,  figsize=[10,10])
    axs[0].imshow(img)
    axs[1].imshow(binary)

    
# Pointset functions
import numpy as np
def image_point_set_txt(file_path):
    """
    from a point_set txt file as generated by itk elastix extract xy coorindates and create a corresponding image
    """
    image = np.zeros([img_dim, img_dim], np.float32)
    with open(file_path, "rt") as myfile: 
        for myline in myfile:            
            string = myline.partition('OutputIndexMoving =')[2]
            string=string.strip()
            string = string.strip('[]')
            string=string.strip()
            y,x = string.split()
            image[int(x),int(y)] = 1
    return image

def image_point_set_object(transformix_object, shape):
    """
    from a point_set object as generated by itk elastix extract xy coorindates and create a corresponding binary image
    """
    image = np.zeros([shape[0], shape[1]], np.float32)  
    # This is the index not sure if it is base 1 or 0
    idx = np.where(transformix_object[0] == 'OutputIndexMoving')[0][0]
    for res in transformix_object:
        y = res[idx+3]
        x = res[idx+4]
        # Sometime transform may exceed the boundary of image!
        if (int(x) < shape[0]) and (int(y) < shape[1]):
            image[int(x),int(y)] = 1

    return image

def image_point_set_input(x, y, scaling):
    """
    From a point set defined by a x and y vector (physical coordinates) create a correspoding binary image 
    """
    image = np.zeros([img_dim, img_dim], np.float32)
    for i in range(len(x)):
        image[int(x[i]/scaling), int(y[i]/scaling)] = 1
    return image

def point_set_to_file(x, y, file_path, spacing, write_nrpoints):
    """
    export a point set defined by x and y in pixels to a file with scaled coordinates.
    File can be used for a forward transform in elastix
    """
    point_set = open(file_path, "w+")
    if write_nrpoints:
        point_set.write("point\n%d\n" % len(x))
    for i in range(0, len(x)):
        point_set.write("%.2f %.2f\n" % (x[i]*spacing, y[i]*spacing))
    point_set.close()

def point_set_object_to_file(transformix_object, file_path):
    """
    export a point set object to a file. 
    File can be used for a forward transform
    """
    point_set = open(file_path, "w+")
    idx = np.where(transformix_object[0] == 'OutputPoint')[0][0]
    for res in transformix_object: 
        y = res[idx+3]
        x = res[idx+4]
        point_set.write("%.4f %.4f\n" % (float(x), float(y)))
    point_set.close()