#@ File post

run("Close All");
basename = File.getNameWithoutExtension(post);
path = File.getDirectory(post);
open(path + basename + "_affine.tif");
rename("affine");
open(path + basename + "_affine_bspline.tif");
rename("affine_bspline");
open(path + basename + "_simil.tif");
rename("simil")
open(path + basename + "_simil_bspline.tif");
rename("simil_bspline")

run("Merge Channels...", "c2=affine c6=affine_bspline create keep");
run("Enhance Contrast", "saturated=0.35");
getMinAndMax(min, max);
max = max*0.6;
setMinAndMax(min, max);
Stack.setChannel(2);
setMinAndMax(min, max);
run("Flatten");
rename("Overlay_affine")

selectImage("affine");;
setMinAndMax(min, max);
run("RGB Color");
selectImage("affine_bspline");
setMinAndMax(min, max);
run("RGB Color");
run("Concatenate...", "open image1=affine image2=affine_bspline image3=Overlay_affine image4=[-- None --]");
run("Make Montage...", "columns=3 rows=1 scale=1 border=6");
rename("montage_affine");


saveAs("jpg", path + basename + "_montage_affine.jpg");

////
run("Merge Channels...", "c2=simil c6=simil_bspline create keep");
setMinAndMax(min, max);
Stack.setChannel(2);
setMinAndMax(min, max);
run("Flatten");
rename("Overlay_simil")

selectImage("simil");;
setMinAndMax(min, max);
run("RGB Color");
selectImage("simil_bspline");
setMinAndMax(min, max);
run("RGB Color");
run("Concatenate...", "open image1=simil image2=simil_bspline image3=Overlay_simil image4=[-- None --]");
run("Make Montage...", "columns=3 rows=1 scale=1 border=6");
rename("montage_simil");


saveAs("jpg", path + basename + "_montage_simil.jpg");